#!/bin/bash

# Check to see if a pipe exists on stdin.
if [ -p /dev/stdin ]; then
    { IFS= read -rd '' pipe_in </dev/stdin; } 2>/dev/null
else
    echo "No input was found on stdin, skipping!"
    exit 1
fi

{ IFS= read -rd '' template_in <./template.html; } 2>/dev/null

title="$1"
description="$2"

result="${template_in//'{TITLE}'/$title}"
result="${result//'{DESCRIPTION}'/$description}"
result="${result//'{CONTENT}'/$pipe_in}"

echo "$result" > ./index.html